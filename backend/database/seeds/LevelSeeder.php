<?php

use App\Level;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                "id" => 1,
                "name" => "Анхан",
                "img" => "/temp/business.png",
                "description" => "Стартап, бикнес төсөл"
            ],
            [
                "id" => 2,
                "name" => "Анхан дунд",
                "img" => "/temp/technology.jpg",
                "description" => "Вэб сайт хийх, Апп бүтээх"
            ],
            [
                "id" => 4,
                "name" => "Дунд",
                "img" => "/temp/self-development.jpg",
                "description" => "Хандлага, харилцаа"
            ],
            [
                "id" => 5,
                "name" => "Ахисан дунд",
                "img" => "/temp/self-development.jpg",
                "description" => "Хандлага, харилцаа"
            ],
            [
                "id" => 6,
                "name" => "Мэргэжлийн",
                "img" => "/temp/self-development.jpg",
                "description" => "Хандлага, харилцаа"
            ],
        ];


        foreach ($items as $item) {
            Level::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
