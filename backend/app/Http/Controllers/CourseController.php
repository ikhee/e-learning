<?php

namespace App\Http\Controllers;

use File;
use App\Http\Requests\CourseRequest;
use App\Http\Requests\UploadFile;
use App\Models\Course;
use App\Http\Resources\CourseResource;
use App\Http\Resources\DetailCourseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{

    protected $course;

    public function __construct(Course $course)
    {
        $this->course = $course;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::when(request('category_id', '') != '', function ($query) {
            $query->where('category_id', request('category_id'));
        })->when(request('user_id', '') != '', function ($query) {
            $query->where('user_id', request('user_id'));
        })->when(request('student_id', '') != '', function ($query) {
            $query->whereIn('id', function ($query1) {
                $query1->select('course_id')->from('check_transactions')->where('user_id', Auth::user()->id);
            });
        });
        return DetailCourseResource::collection($courses->paginate(8));
    }

    public function getowncourse()
    {
        // return CourseResource::collection(Course::get());
        return DetailCourseResource::collection(Course::where('user_id', Auth::user()->id)->paginate(15));
    }

    public function getNewCourse()
    {
        return DetailCourseResource::collection(Course::orderBy('created_at', 'DESC')->take(8)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $image = $request->src;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = Str::random(30) . '.png';
        $request->src = $imageName;
        $request->buyCount = 0;
        Storage::disk('public')->put($imageName, base64_decode($image));
        $path = Storage::disk('public')->path("{$imageName}");
        File::move($path, "course/{$imageName}");
        $data = new DetailCourseResource($this->course::store($request));
        return $data;
        // return ["Result" => $request->title];
    }

    public function uploadFile(UploadFile $request)
    {

        $file = $request->file('file');
        $path = Storage::disk('public')->path("chunks/{$file->getClientOriginalName()}");
        File::append($path, $file->get());
        if ($request->has('is_last') && $request->input('is_last') == true) {
            $name = basename($path, '.part');
            File::move($path, "video/{$name}");
            return response()->json(['file' => true]);
        }
        return response()->json(['uploaded' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new CourseResource(Course::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flight = Course::find($id);
        $flight->delete();
    }
}
