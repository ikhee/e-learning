<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PhotoUploadRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File;

class PhotoUploadController extends Controller
{
    public function movePhoto(PhotoUploadRequest $request) {
        $image = $request->photos;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        // return base64_decode($image);
        $imageName = Str::random(30) . '.png';
        $path = Storage::disk('public')->put($imageName, base64_decode($image));
        return response()->json(['success'=>$path, 'file'=>$imageName]);
        // return Storage::disk('public')->get('Rw98hfKyBjXiRrofgZXmIIFcR7rt3T.png');
        // return Storage::disk('public')->url('Rw98hfKyBjXiRrofgZXmIIFcR7rt3T.png');
    }
}
