<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserLoginRequest;
class AuthController extends Controller{
    public function register(UserRegisterRequest $request) {

        User::create([
            'email' => $request->email,
            'login_name' => $request->login_name,
            'role' => $request->role,
            'first_name' => $request->first_name,
            'password' => bcrypt($request->password),
        ]);

        if (!$token = auth()->attempt($request->only(['login_name','password']))){
            return abort(401);
        };
        return (new UserResource($request->user()))->additional([
            'meta' => [
                'token' => $token,
            ],
        ]);
    }
    public function login(UserLoginRequest $request) {
        if (!$token = auth()->attempt($request->only(['login_name','password']))){
            return response()->json([
                'errors' => [
                    'password' => ['Уучлаарай, хэрэглэгчийн бүртгэл олдсонгүй.'],
                ],
            ], 422);
        };
        return (new UserResource($request->user()))->additional([
            'meta' => [
                'token' => $token,
            ],
        ]);
    }

    public function user(Request $request) {
        return new UserResource($request->user());
    }

    public function logout() {
        auth() -> logout();
    }
}
