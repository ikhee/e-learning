<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class DetailCourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'user_id' => $this->user_id,
            'src' => $this->src,
            'teacher' => new UserResource($this->user),
            'price' => $this->price,
            'sell_percent' => $this->sell_percent,
            'buyCount' => $this->buyCount,
            'category' => new CategoryResource($this->category),
            'description'=> $this->description,
            'level' => new LevelResource($this->level),
            'language_id' => $this->language_id,
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
