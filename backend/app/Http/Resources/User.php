<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent :: toArray($request);
        return [
            'id' => $this->id,
            'role' => $this->role,
            'login_name' => $this->login_name,
            'first_name' => $this->first_name,
            'email' => $this->email
        ];
    }
}
