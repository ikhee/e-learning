<?php

namespace App\Http\Resources;

use App\CheckTransaction;
use App\Models\Course;
use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent :: toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'course_id' => $this->group->course_id,
            'video_url' => ($this->is_free == 1) ? $this->video_url : ($this->checkPermissionVideo($this->group->course_id) ? $this->video_url : false),
            // 'video_url' => ($this->is_free == 1) ? $this->video_url : ($this->checkPermissionVideo($this->group->course_id)),
            'description' => $this->description,
            'expired_time' => $this->expired_time,
            'is_free' => $this->is_free,
            'type_id' => $this->type_id,
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }

    private function checkPermissionVideo($course_id)
    {
        if (Auth::user()) {
            if (Auth::user()->role == 'admin') {
                // Админ ямарч хичээлийг үзэх боломжтой.
                return true;
            }
            $tran = CheckTransaction::where('course_id', '=', $course_id)->where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->first();
            if ($tran == null) {
                // Сургалт үүсгэсэн хэрэглэгч өөрөө хандах үед
                $course = Course::where('id', '=', $course_id)->where('user_id', '=', Auth::user()->id)->first();
                if ($course != null) {
                    return true;
                }
                // Худалдаж аваагүй.
                return false;
            } else {
                $datetime1 = new DateTime();
                $datetime2 = new DateTime($tran->updated_at);
                $interval = $datetime1->diff($datetime2);
                $days = $interval->format('%a');
                if ($tran->expiry_day > $days) {
                    // Худалдаж авсан хүчинтэй.
                    return true;
                }
                // Худалдаж авсан хугацаа нь дууссан.
                return false;
            }
        } else {
            // Системд нэвтрээгүй.
            return false;
        }
    }
}
