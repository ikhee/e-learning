<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'user_id' => $this->user_id,
            'user' => new User($this->user),
            'groups' => new GroupsResource($this->group),
            'language_id' => $this->language_id,
            'level_id' => $this->level_id,
            'src' => $this->src,
            'price' => $this->price,
            'sell_percent' => $this->sell_percent,
            'buyCount' => $this->buyCount,
            'category_id' => $this->category_id,
            'description'=> $this->description,
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
