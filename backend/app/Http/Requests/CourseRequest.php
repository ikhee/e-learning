<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            if (Auth::user()->role == 'admin' || Auth::user()->role == 'teacher') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'src' => 'required',
            'price' => 'required',
            'sell_percent' => 'required',
            'category_id' => 'required',
            'description'=> 'required',
            'level_id'=> 'required',
            'language_id'=> 'required',
        ];
    }
}
