<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required',
            'role' => 'required',
            'first_name' => 'required',
            'password' => 'required|min:6',
            'login_name' => 'required|unique:users,login_name'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'required',
            'first_name.required' => 'required',
            'email.email' => 'Мэйл хаягаа зөв оруулна уу.',
            'email.unique' => 'Энэ мэйл хаягаар өмнө нь хүн бүртгүүлсэн байна',
            'password.min' => 'Нууц үгний урт 6-с илүү байх ёстой.',
            'role.required' => 'Систем хэрэглэгчийн үүргийг тодорхойлно уу',
            'login_name.unique:users,login_name' => 'Нэвтрэх нэр бүртгэлтэй байна.'
        ];
    }
}
