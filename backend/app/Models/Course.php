<?php

namespace App\Models;

use App\Category;
use App\Group;
use App\Level;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'src',
        'buyCount',
        'price',
        'category_id',
        'description'
    ];

    public static function store($request){
        $course = new self;
        $course->title = $request->title;
        $course->user_id = $request->user()->id;
        $course->src = $request->src;
        $course->price = $request->price;
        $course->sell_percent = $request->sell_percent;
        $course->level_id = $request->level_id;
        $course->language_id = $request->language_id;
        $course->category_id = $request->category_id;
        $course->description = $request->description;
        $course->save();
        return $course;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function content(){
        return $this->hasMany(Content::class);
    }

    public function group(){
        return $this->hasMany(Group::class);
    }

    public function level(){
        return $this->belongsTo(Level::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

}
