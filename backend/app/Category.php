<?php

namespace App;

use App\Models\Course;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'img',
        'icon',
        'description'
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
