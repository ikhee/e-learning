<?php

namespace App;

use App\Models\Content;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'course_id'
    ];

    public static function store($request){
        $course = new self;
        $course->name = $request->name;
        $course->order_no = $request->order_no;
        $course->course_id = $request->course_id;
        $course->save();
        return $course;
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function content(){
        return $this->hasMany(Content::class);
    }
}
