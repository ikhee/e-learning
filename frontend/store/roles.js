export const state = () => ({
  roles: {
    ADMIN: 'admin',
    TEACHER: 'teacher',
    STUDENT: 'student'
  }
})

// getters
export const getters = {
  role: state => state.roles,
}
