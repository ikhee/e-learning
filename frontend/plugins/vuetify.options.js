export default function ({ app }) {
  return{ 
    lang: {
      ln: (key, ...params) => app.i18n.ln(key, params)
    }
  }
}